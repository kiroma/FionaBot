const {prefix, token} = require('./config.json');
const Discord = require('discord.js');

const fs = require('fs');

const client = new Discord.Client();
client.commands = new Discord.Collection();
global.vcConnections = [];

const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for(const file of commandFiles) {
	const command = require(`./commands/${file}`);
	client.commands.set(command.name, command);
}

client.on('ready', () => {
	console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', async message => {
	if (!message.content.startsWith(prefix) || message.author.bot)
	{
		return;
	}
	const args = message.content.slice(prefix.length).trim().split(/ +/);
	const commandName = args.shift().toLowerCase();
	
	if(!client.commands.has(commandName)) {
		message.reply('Unrecognized command');
		return;
	}
	
	const command = client.commands.get(commandName);
	
	try {
		command.execute(message, args);
	} catch(error) {
		console.error(error);
		message.reply(`Error executing command ${commandName}!`);
		return;
	}
});

client.on('guildMemberAdd', async member => {
	const channel = member.guild.channels.cache.find(ch => ch.name === 'general');
	if(!channel)
	{
		console.log('Error: Could not find a suitable greetings channel!');
		return;
	}
	channel.send(`Welcome to the server, ${member}`);
});

client.login(token);
