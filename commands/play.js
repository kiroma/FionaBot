const ytdl = require('ytdl-core');

module.exports = {
	name: 'play',
	description: 'Plays an url in the currently connected voice channel',
	async execute(message, args) {
		const vcConnection = global.vcConnections[message.member.voice.channel];
		if(!vcConnection) {
			message.reply("Join me to a voice channel first!");
			return;
		}
		for(const arg of args) {
			if(!ytdl.validateURL(arg)){
				message.channel.send(`Could not open ${arg}`);
				continue;
			}
			vcConnection.queue.push(ytdl(arg, { filter: 'audioonly' }));
		}
		if(!vcConnection.playing) {
			vcConnection.playing = true;
			const nextVid = vcConnection.queue.shift();
			if(!nextVid)
			{
				return;
			}
			const connection = await vcConnection.connection;
			const dispatcher = connection.play(nextVid);
			let callback = () => {
				const nextItem = vcConnection.queue.shift();
				if(nextItem) {
					const dispatcher = vcConnection.connection.play(nextItem);
					dispatcher.on('finish', callback);
				}
				else {
					vcConnection.playing = false;
				}
			}
			dispatcher.on('finish', callback);
		}
	}
}
