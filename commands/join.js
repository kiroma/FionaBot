module.exports = {
	name: 'join',
	description: 'Joins the voice chat of the user',
	execute(message, args) {
		const voiceChannel = message.member.voice.channel;
		if(voiceChannel) {
			if(global.vcConnections[voiceChannel]) {
				message.reply("Already joined!");
				return;
			}
			global.vcConnections[voiceChannel] = new Object();
			global.vcConnections[voiceChannel].queue = [];
			global.vcConnections[voiceChannel].playing = false;
			global.vcConnections[voiceChannel].connection = voiceChannel.join();
		} else {
			message.reply('You need to join a voice channel first!');
		}
	}
}
