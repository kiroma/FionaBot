module.exports = {
	name: 'leave',
	description: 'leaves the currently joined vc',
	execute(message, args) {
		const voiceChannel = message.member.voice.channel;
		if(!voiceChannel) {
			message.reply("You're not in a voice channel!");
			return;
		}
		const vcConnection = global.vcConnections[voiceChannel];
		if(!vcConnection) {
			return;
		}
		vcConnection.connection.then(connection => {
			connection.disconnect();
		});
		global.vcConnections[voiceChannel] = null;
	}
}
